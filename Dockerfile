# Docker file developed by Ivan Hugo Guevara - PhD Student (University of Limerick)
FROM ubuntu:latest

RUN apt-get remove

RUN apt-get update

RUN apt-get install -y autoconf m4 git make build-essential texinfo clang wget

RUN wget https://ftp.gnu.org/gnu/automake/automake-1.16.tar.gz

RUN tar -xvf automake-1.16.tar.gz && cd automake-1.16 && ./configure && make && make install

# These commands copy your files into the specified directory in the image
COPY . /home/BDS
WORKDIR /home/BDS

# This command compiles your app using GCC, adjust for your source code
RUN cd /home/BDS/ga && make && make install
RUN cd /home/BDS/ga/examples && make
RUN cd /home/BDS/libge && aclocal && autoconf && automake && ./configure && make && make install


# This command runs your application, comment out this line to compile only

LABEL Name=libge-docker Version=0.0.1
