// GEprime.c -*- C -*-

#ifndef _GEPRIME_C_
#define _GEPRIME_C_
#define ll long long

#include <stdio.h>
#include <math.h>
#include <time.h>
#include<stdlib.h>

// declaring required variables
int n = 2;
unsigned int currFitCase = 0; //current fitness value
unsigned int sumfit =0;
unsigned int penalty =10;
unsigned int hitcount = 0; //count of valid individuals
int prime = 0;
int l = 2;
int r = 10;
int numfitcase = 5;


// function declaration
int create_rand();
int brute_force(int n); 
int miller(int n ); 
int fermat(int n ); 
int fitness(int prime);

// Function prototypes

int create_rand()
{
srand(time(0));
return (rand() % (r - l + 1)) + l;
}

int brute_force(int n)
{
for (int i = 2; i <= n / 2; ++i) {
        // condition for non-prime
        if (n % i == 0) {
            prime = 1;
            break;
        }
    }
return prime;
}


ll modulo(ll base, ll exponent, ll mod) {
    ll x = 1;
    ll y = base;
    while (exponent > 0) {
        if (exponent % 2 == 1)
            x = (x * y) % mod;
        y = (y * y) % mod;
        exponent = exponent / 2;
    }
    return x % mod;
}

long long mulmod(long long a, long long b, long long mod)
{
    long long x = 0,y = a % mod;
    while (b > 0)
    {
        if (b % 2 == 1)
        {    
            x = (x + y) % mod;
        }
        y = (y * 2) % mod;
        b /= 2;
    }
    return x % mod;
}
 
int fermat(int n )
{
    if (n == 1) {
        return 0;
    }
        ll a = rand() % (n - 1) + 1;
        if (modulo(a, n - 1, n) != 1) {
            return 0;
        }
    return 1;
}


int miller(int n)
{
 
    int i;
    long long s;
    if (n < 2)
    {
        return 0;
    }
    if (n != 2 && n % 2==0)
    {
        return 0;
    }
     s = n - 1;
    while (s % 2 == 0)
    {
        s /= 2;
    }
    for (i = 0; i < 10; i++)
    {
        long long a = rand() % (n - 1) + 1, temp = s;
        long long mod = modulo(a, temp, n);
        while (temp != n - 1 && mod != 1 && mod != n - 1)
        {
            mod = mulmod(mod, mod, n);
            temp *= 2;
        }
        if (mod != n - 1 && temp % 2 == 0)
        {
            return 0;
        }
    }
    return 1;
}


int fitness(int prime)
{
if (prime==1)
{
hitcount++;
currFitCase++;
}
else if(prime ==0)
{
sumfit+=penalty;
currFitCase++;
}
return sumfit;
}

#endif


