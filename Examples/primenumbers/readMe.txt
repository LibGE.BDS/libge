This is a simple example of evolving addition of three variables a, b and c with the value of addition not exceeding 20.
There is wrapping added to avoid invalid cases, with population size as 70, crossover rate 0.8 and generations as 6 and mutation probability as 0.01 after experimentation.
The minimum time obtained was 22 seconds for the best individual with he above provided conditions after 119 runs.

Files:

1. GEaddition.c
 This file contains the GE part of the equation. The variables are inialized with some random values less than 20. The error of the equation will be simply the addition of a, b and c and will be compared with the maxHitError, 20. If the generated genome satsfies the condition of being less than 20, the number of fitness cases will be incremented by 1. 

2. additionstart.c
 This file contains the declaration of the variables and functions required by the problem. The while loop continues for the range of Fitness cases(in my example it is 6), and left incomplete for the phenotype to generate. The rest part is completed in additionend.c on appending the phenotype.

3. additionend.c
 This file completes the rest part of additionstart.c.

