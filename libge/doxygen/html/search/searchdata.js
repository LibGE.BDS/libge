var indexSectionsWithContent =
{
  0: "_abcdefghilmnoprstux~",
  1: "acgimprst",
  2: "acgilmprst",
  3: "abcefgilmoprstu~",
  4: "acdefgilmprstux",
  5: "acdf",
  6: "ast",
  7: "cfnpt",
  8: "o",
  9: "_beghlmpsu"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "related",
  9: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Friends",
  9: "Macros"
};

