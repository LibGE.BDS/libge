// GEGrammar.h -*- C++ -*-

#pragma once

/*------------------------------------------------------------------------------
 * This class implements the standard GE mapping process. It implements the
 * virtual methods genotype2phenotype and phenotype2genotype, and the
 * standard GE wrapping operator.
------------------------------------------------------------------------------*/


#include <stack>
#include <vector>

#include "libGEdefs.h"
#include "CFGrammar.h"
#include "AGSymbol.h"
#include "AGLookUp.h"
#include "AGContext.h"
#include "AGDerivationTree.h"


using namespace std;



/* ---- GEGrammar ---- */
class GEGrammar : public CFGrammar
{

	public:
		unsigned int getMaxWraps() const;
		void setMaxWraps(const unsigned int);
		bool getUseAG() const;
		const DerivationTree* getDerivationTree();
		const AGDerivationTree* getAGDerivationTree() const;
		const vector<Production*>* getProductions();
		const AGLookUp* getAGLookUp() const;
		const AGContext* getAGContext() const;
		GEGrammar (bool useAttributeGrammar=false, AGLookUp* lookup=NULL,
				AGContext* context=NULL);
		GEGrammar (const Genotype &, bool useAttributeGrammar=false,
				AGLookUp* lookup=NULL, AGContext* context=NULL);
		GEGrammar (const Phenotype &, bool useAttributeGrammar=false,
				AGDerivationTree* newAGDTree=NULL, AGLookUp* lookup=NULL,
				AGContext* context=NULL);
		GEGrammar (const GEGrammar&);
		virtual ~GEGrammar();

	protected:
		bool genotype2Phenotype();
		bool phenotype2Genotype();
    bool useAG;
		void buildDTree (DerivationTree&, vector<Production*>::iterator&);
    int chooseProduction(int codon, Rule *rulePtr, AGTree* =NULL);
		vector <Production*> productions;
    AGLookUp* agLookUp;
		AGContext* agContext;
		AGDerivationTree* agDtree;
		virtual AGMapState buildAGTree (bool init, AGTree &currentNode,
				const Symbol* nodeSym, AGContext& context, vector <const
				Symbol*>& nonterminals, Genotype::iterator& genoIt,
				unsigned int& newEffectiveSize, unsigned int& wraps, bool& gotToUseWrap,
				int prevProdIndex, unsigned int backtrack, const bool growMethod=false,
				const unsigned int maxDepth=0, int genoLenSoFar=0);
		virtual AGMapState getAGInitProductionSet (Rule* &rulePtr,
				AGTree& currentAGNode, const unsigned int maxDepth, const bool
				growMethod, vector<int> &possibleRules);
		virtual Symbol* createSymbol (const Symbol&, bool =false);

	private:
		unsigned int maxWraps;
		int genotype2PhenotypeStep (vector<const Symbol*>&, Genotype::iterator,
				bool, Rule* &, int&, AGTree* =NULL);
		bool genotype2Phenotype (const bool);
		bool geno2phenoStepDecision (int result, Genotype::iterator& genoIt,
				unsigned int& newEffectiveSize, bool& gotToUseWrap, unsigned int& wraps);

};



