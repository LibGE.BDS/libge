//GenericAGSymbol.cpp -*- C++ -*-
#ifndef _GENERICAGSYMBOL_CPP_
#define _GENERICAGSYMBOL_CPP_

#include <iostream>
#include <string>

#include "GenericAGSymbol.h"

/* ---- Constructor: passes on the arguments to superclass ---- */
GenericAGSymbol :: GenericAGSymbol (const string newArray, const SymbolType newType) : AGSymbol (newArray, newType)
{
#if (DEBUG_LEVEL >= 2)
	cerr << "'GenericAGSymbol::GenericAGSymbol(const string, SymbolType)' called\n";
#endif
}




/* ---- Copy constructor ---- */
GenericAGSymbol :: GenericAGSymbol (const GenericAGSymbol &copy) : AGSymbol(copy)
{
#if (DEBUG_LEVEL >= 2)
 	cerr << "'GenericAGSymbol::GenericAGSymbol(const GenericAGSymbol&)' called\n";
#endif
}




/* ---- Destructor ---- */
GenericAGSymbol :: ~GenericAGSymbol()
{
#if (DEBUG_LEVEL >= 2)
	cerr << "'GenericAGSymbol::~GenericAGSymbol()' called\n";
#endif
}

 /* Patch from C++17 to avoid warnings for using polymorphic functions - We need to check default values */

/* ---- A default implementation; only returns success ---- */
[[maybe_unused]] AGMapState GenericAGSymbol :: updateInheritedAttributes( [[maybe_unused]] AGContext &context, [[maybe_unused]]  const int prodIndex, [[maybe_unused]] AGLookUp& lookUp)
{
#if (DEBUG_LEVEL >= 2)
	cerr << "'GenericAGSymbol::updateInheritedAttributes(AGContext &, const int, AGLookUp&)' called\n";
#endif

	return PASS;

}



/* ---- A default implementation; only returns success ---- */
[[maybe_unused]] AGMapState GenericAGSymbol :: updateSynthesisedAttributes ([[maybe_unused]] AGContext &, [[maybe_unused]] const int prodIndex, [[maybe_unused]] AGLookUp& lookUp)
{
#if (DEBUG_LEVEL >= 2)
        cerr << "'GenericAGSymbol::updateSynthesisedAttributes(AGContext &context, const int, AGLookUp&)' called\n";
#endif

	return PASS;

}



/* ---- Copy newValue ---- */
GenericAGSymbol &GenericAGSymbol :: operator = (const GenericAGSymbol newValue)
{

	#if (DEBUG_LEVEL >= 2)
		cerr << "'GenericAGSymbol& GenericAGSymbol :: operator = (const GenericAGSymbol)' called\n";
	#endif

	//Pass it on to the superclass.
	AGSymbol :: operator =( newValue);

	return *this;

}


#endif

