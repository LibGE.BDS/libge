// AGContext.h -*- C++ -*-

#pragma once

/*------------------------------------------------------------------------------
 * This class defines the context of an AGSymbol object while expanding a tree
 * of type AGDerivationTree. When calling the updateInheritedAttributes()
 * method of an AGSymbol object, the 'startIndex' points to the leftmost sibl-
 * ing of the AGSymbol object in the same subtree, whereas the 'endIndex'
 * points to the AGSymbol object itself. 'parent' points to the parent node of
 * the AGSymbol object.
 *
 * When calling the updateSynthsisedAttributes() method of an AGSymbol object
 * the context should contain information on all the child nodes. Thus,
 * 'startIndex' points to the left most child, and 'endIndex' points to the end
 * of the iterator. Note, in this case endIndex=it.end() where 'it' is an iter-
 * ator of type AGTree::iterator; 'endIndex' does NOT point to the last child.
 * 'parent' points to the parent node of the AGSymbol object.
 *
 * GEGrammar::buildAGTree method contains the code to update AGContext object.
------------------------------------------------------------------------------*/

#include "libGEdefs.h"
#include "AGDerivationTree.h"

using namespace std;

class AGContext
{

	public:
		AGContext();
		AGContext(AGTree*, AGTree::iterator, AGTree::iterator);
		AGTree* getParent() const;
		AGTree :: iterator getStartIndex() const;
		AGTree :: iterator getEndIndex() const;
		void setParent(AGTree* p);
		void setStartIndex (AGTree :: iterator);
		void setEndIndex (AGTree :: iterator);
		virtual ~AGContext();

	protected:
		AGTree* parent;
		AGTree :: iterator startIndex;
		AGTree :: iterator endIndex;

};


