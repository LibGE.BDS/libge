//GenericAGSymbol.h -*- C++ -*-

#pragma once

/*------------------------------------------------------------------------------
	This class corresponds to symbols in the grammar file which do not require
	unique attributes and specialised methods for updating attributes.
	Examples are blank space character in this case, but also opening and
	closes braces for some other problems.
------------------------------------------------------------------------------*/


#include "AGSymbol.h"

using namespace std;



/* ---- GenericAGSymbol ---- */
class GenericAGSymbol : public AGSymbol
{

	public:
		GenericAGSymbol (const string, const SymbolType);
		GenericAGSymbol(const GenericAGSymbol&);
		~GenericAGSymbol();
 		GenericAGSymbol &operator = (const GenericAGSymbol);
		virtual AGMapState updateSynthesisedAttributes(AGContext &,
				const int prodIndex, AGLookUp& lookUp);
		virtual AGMapState updateInheritedAttributes(AGContext &,
				const int prodIndex, AGLookUp& lookUp);

};


