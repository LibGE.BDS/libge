// GEGrammarSI.h -*- C++ -*-

#pragma once

/*------------------------------------------------------------------------------
 This class implements the standard GE ramped half-and-half initialisation
 routine, also known as sensible initialisation. It implements the virtual
 methods from the class Initialiser.
------------------------------------------------------------------------------*/

#include "libGEdefs.h"
#include "Initialiser.h"
#include "GEGrammar.h"


using namespace std;



/* ---- GEGrammarSI ---- */
class GEGrammarSI: public GEGrammar, public Initialiser
{

	public:
		float getGrow() const;
		void setGrow(const float);
		float getFull() const;
		void setFull (const float);
		unsigned int getMaxDepth() const;
		void setMaxDepth (const unsigned int);
		float getTailRatio() const;
		void setTailRatio (const float);
		unsigned int getTailSize() const;
		void setTailSize (const unsigned int);
		bool init (const unsigned int=UINT_MAX);
		GEGrammarSI (bool useAttributeGrammar=false, AGLookUp* lookup=NULL,
				AGContext* context=NULL);
		GEGrammarSI (const Genotype &,bool useAttributeGrammar=false,
				AGLookUp* lookup=NULL, AGContext* context=NULL);
		GEGrammarSI (const Phenotype &,bool useAttributeGrammar=false,
				AGDerivationTree* newAGDTree=NULL, AGLookUp* lookup=NULL,
				AGContext* context=NULL);
		GEGrammarSI (const GEGrammarSI&);
		virtual ~GEGrammarSI();

	protected:
		virtual AGMapState getAGInitProductionSet (Rule* &rulePtr,
				AGTree& currentAGNode, const unsigned int maxDepth,
				const bool growMethod, vector<int> &possibleRules);

	private:
		float grow;
		unsigned int maxDepth;
		float tailRatio;
		unsigned int tailSize;
		bool growTree (DerivationTree&,const bool&, const unsigned int&);

};



