// AGSymbol.cpp -*- C++ -*-

#ifndef _AGSYMBOL_CPP_
#define _AGSYMBOL_CPP_

//#include <config.h>
#include <iostream>
#include <string>

#include "AGSymbol.h"



/* ---- Create a new Symbol object as a copy of newArray ---- */
// and with type newType, if specified; otherwise the default values
// specified in the function prototype are used.
AGSymbol :: AGSymbol (const string newArray, const SymbolType newType) : Symbol (newArray, newType)
{

  #if (DEBUG_LEVEL >= 2)
    cerr << "'AGSymbol::AGSymbol(const string, const SymbolType)' called\n";
  #endif

}



/* ---- Copy constructor ---- */
AGSymbol :: AGSymbol (const AGSymbol &copy)
{

  #if (DEBUG_LEVEL >= 2)
    cerr << "'AGSymbol::AGSymbol(const AGSymbol&)' called\n";
  #endif

  append(copy);
  setType(copy.getType());

}



/* ---- Destructor ---- */
AGSymbol :: ~AGSymbol()
{
  #if (DEBUG_LEVEL >= 2)
    cerr << "'AGSymbol::~AGSymbol()' called\n";
  #endif
}



/* ---- Symbol comparison operator ---- */
bool AGSymbol :: operator == (const AGSymbol &newSymbol)
{

  #if (DEBUG_LEVEL >= 2)
    cerr << "'bool AGSymbol::operator==(const AGSymbol&)' called\n";
  #endif

  return Symbol::operator==(newSymbol);

}



/* ----  Used for recycling AGSymbol objects ---- */
// This method shows whether this object is currently being used
// by a derivation tree or is available for claiming.
bool AGSymbol :: getInUse() const
{

  #if (DEBUG_LEVEL >= 2)
    cerr << "'bool AGSymbol::getInUse() const' called\n";
  #endif

  return inUse;

}



/* ---- Used for recycling AGSymbol objects ---- */
// This method sets the inUse status of this object to show whether
// the object is currently being used by a derivation tree
// or available for claiming.
void AGSymbol :: setInUse (bool use)
{

  #if (DEBUG_LEVEL >= 2)
    cerr << "'void AGSymbol::setInUse(bool)' called\n";
  #endif

  inUse=use;

}



/* ---- This method enables forward checking by allowing each AGSymbol ---- */
// to have a list of valid productions that can be used to expand
// this Non-Terminal.
// This method returns NULL for the terminal symbols for obvious
// reasons.
// For the non-terminals, the classes extending AGSymbol should
// override this method.
// If forward checking is not used, there is no need to override
// this method.
const vector <bool> * AGSymbol :: getValidProductionIndices() const
{

  #if (DEBUG_LEVEL >= 2)
    cerr << "'void AGSymbol::getValidProductionIndices()' called\n";
  #endif

  return NULL;

}


#endif

