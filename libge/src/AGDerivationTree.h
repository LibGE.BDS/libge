// Tree.h -*- C++ -*-
#pragma once

/*------------------------------------------------------------------------------
 This class implements an n-ary derivation tree containing AGSymbol*.
------------------------------------------------------------------------------*/



#include "Tree.h"
#include "AGSymbol.h"
#include "Phenotype.h"

using namespace std;


enum TraversalReason
{
  CLEAR, PRINT, PHENOTYPE
};



typedef Tree <AGSymbol*> AGTree;




class AGDerivationTree
{

	public:
		AGDerivationTree (bool recycleNodes=false);
		AGDerivationTree (const AGDerivationTree &copy);
		virtual ~AGDerivationTree();
		virtual bool isRecycling() const;
		virtual void setTree (AGTree* newTree);
		virtual AGTree* getTree() const;
		virtual void clearTree();
		virtual void printTree();
		virtual void loadPhenotype (Phenotype* pheno);
		static void clearSubtreeBelow (AGTree& node, bool recycleNodes);

	protected:
		AGTree* tree;
		bool recycle;
		static void traverseTree (AGTree& currentNode, TraversalReason reason,
				bool recycleNodes, Phenotype* pheno=NULL);

};




