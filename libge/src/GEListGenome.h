// GEListGenome.h -*- C++ -*-



#pragma once

#include <ga/ga.h>



/* ---- GEListGenome ---- */
class GEListGenome:public GAListGenome <unsigned char>
{

	public:
	  GEListGenome();
  	GEListGenome(const GEListGenome& source);
  	GEListGenome operator=(const GAGenome& source);
  	int getEffectiveSize() const;
  	double getTestFitness() const;
  	void setEffectiveSize (const int newEffSize);
  	void setTestFitness (const double tFitness);
  	static int pointMutator (GAGenome& g, float pmut);
  	static int effCrossover (const GAGenome& p1, const GAGenome& p2,
				GAGenome* c1, GAGenome* c2);
  	virtual ~GEListGenome();
  	virtual GEListGenome* clone (GAGenome::CloneMethod method) const;
  	virtual void copy (const GAGenome& source);
  	virtual int equal (const GAGenome& source) const;

	private:
  	void helpCopy (const GEListGenome& source);
  	int helpCompare (const GEListGenome& source) const;
  	unsigned int effSize;
  	double testFitness;

};


