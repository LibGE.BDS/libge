// GEGrammar.cpp -*- C++ -*-

#ifndef _GEGRAMMAR_CPP_
#define _GEGRAMMAR_CPP_

//#include <config.h>
#include <cstdio>
#include <iostream>
#include <sstream>
#include <cstring>
#include <vector>
#include <stack>

#include "GEGrammar.h"




/* ---- Default constructor ---- */
GEGrammar :: GEGrammar (bool useAttributeGrammar, AGLookUp* lookup, AGContext* context) : CFGrammar()
{

  #if (DEBUG_LEVEL >= 2)
    cerr << "'GEGrammar::GEGrammar()' called\n";
  #endif

  setMaxWraps(0);
  useAG=useAttributeGrammar;

  if (useAG)
  {
    agDtree = new AGDerivationTree();
    if (lookup==NULL || context==NULL)
    {
      cerr << "GEGrammar :: GEGrammar() called with useAG=true but lookup = NULL or context = NULL" << endl;
      exit(1);
    }
    agLookUp=lookup;
    agContext=context;
    Production::setAGLookUp(lookup);
  }

}



/* ---- Constructor setting the genotype structure ---- */
// of this mapper to newGenotype.
GEGrammar :: GEGrammar (const Genotype &newGenotype, bool useAttributeGrammar, AGLookUp* lookup, AGContext* context) : CFGrammar (newGenotype)
{

  #if (DEBUG_LEVEL >= 2)
    cerr << "'GEGrammar::GEGrammar(const Genotype &, bool=false, AGLookUp*=NULL, AGContext*=NULL))' called\n";
  #endif

  setMaxWraps(0);
  useAG=useAttributeGrammar;

  if (useAG)
  {
    if (lookup==NULL || context==NULL)
    {
      cerr << "GEGrammar::GEGrammar(const Genotype &, bool=false, AGLookUp*=NULL, AGContext*=NULL) called with useAG=true but lookup=NULL or context=NULL" << endl;
      exit(1);
    }
    agLookUp=lookup;
    agContext=context;
  }

}



/* ---- Constructor setting the phenotype structure ---- */
// of this mapper to newPhenotype.
GEGrammar :: GEGrammar (const Phenotype &newPhenotype, bool useAttributeGrammar, AGDerivationTree* newAGDTree, AGLookUp* lookup, AGContext* context) : CFGrammar (newPhenotype)
{

  #if (DEBUG_LEVEL >= 2)
    cerr << "'GEGrammar::GEGrammar(const Phenotype &, bool=false, AGLookUp*=NULL, AGContext*=NULL))' called\n";
  #endif

  setMaxWraps(0);
  useAG=useAttributeGrammar;

  if (useAG)
  {
    if (newAGDTree==NULL || lookup==NULL || context==NULL)
    {
      cerr << "GEGrammar::GEGrammar(const Phenotype &, bool=false, AGLookUp*=NULL, AGContext*=NULL) called with useAG=true but newAGTree=NULL or lookup=NULL or context=NULL"<<endl;
      exit(1);
    }
    agLookUp=lookup;
    agContext=context;
    agDtree=newAGDTree;
  }

}



/* ---- Copy Constructor ---- */
GEGrammar :: GEGrammar (const GEGrammar& copy) : CFGrammar(copy)
{

  #if (DEBUG_LEVEL >= 2)
    cerr << "'GEGrammar::GEGrammar(const GEGrammar&)' called\n";
  #endif

  setMaxWraps(copy.getMaxWraps());
  useAG=copy.getUseAG();

  // Call genotype2Phenotype() to regenerate phenotype structure,
  // and productions and derivationTree structures.
  if (useAG) agDtree=new AGDerivationTree();
  *agLookUp=*(copy.getAGLookUp());
  *agContext=*(copy.getAGContext());

  #if (DEBUG_LEVEL >= 1)
    cerr << " Calling genotype2phenotype() from 'GEGrammar::GEGrammar(const GEGrammar&)'\n";
  #endif

  genotype2Phenotype(true);

}



/* ---- Destructor ---- */
GEGrammar :: ~GEGrammar()
{

  #if (DEBUG_LEVEL >= 2)
    cerr << "'GEGrammar::~GEGrammar()' called\n";
  #endif

  if (agDtree!=NULL)  delete agDtree;
  if (agLookUp!=NULL) delete agLookUp;
  if (agContext!=NULL) delete agContext;

}



/* ---- Return number of maximum allowed wrapping events ---- */
unsigned int GEGrammar :: getMaxWraps() const
{

  #if (DEBUG_LEVEL >= 2)
    cerr << "'unsigned int GEGrammar::getMaxWraps() const' called\n";
  #endif

  return maxWraps;

}



/* ---- Set the new number of maximum allowed wrapping events ---- */
void GEGrammar :: setMaxWraps (const unsigned int newMaxWraps)
{

  #if (DEBUG_LEVEL >= 2)
    cerr << "'void GEGrammar::setMaxWraps(const unsigned int)' called\n";
  #endif

  maxWraps=newMaxWraps;

}



/* ---- Returns whether Attribute Grammars are in use ---- */
bool GEGrammar :: getUseAG() const
{

  #if (DEBUG_LEVEL >= 2)
    cerr << "'bool GEGrammar::getUseAG() const' called\n";
  #endif

  return useAG;

}



/* ---- Builds the current derivation tree ---- */
// and returns its address; if derivation tree is impossible to build,
// e.g. when useAG=true, returns NULL.
const DerivationTree* GEGrammar :: getDerivationTree()
{

  #if (DEBUG_LEVEL >= 2)
    cerr << "'const DerivationTree* GEGrammar::getDerivationTree()' called\n";
  #endif

  if ((!getValidGrammar()) || (!genotype.getValid()) || (!getGenotype()-> size()) || getUseAG())
  {
    return NULL;
  }

  #if (DEBUG_LEVEL >= 1)
    cerr << " Calling genotype2phenotype() from 'GEGrammar::getDerivationTree()'\n";
  #endif

  genotype2Phenotype (true);
  return &derivationTree;

}



/* ---- Returns agDtree ---- */
const AGDerivationTree* GEGrammar :: getAGDerivationTree() const
{

  #if (DEBUG_LEVEL >= 2)
    cerr << "'AGDerivationTree* GEGrammar::getAGDerivationTree()' called\n";
  #endif

  return agDtree;

}



/* ---- Returns a vector of all productions ---- */
// used during the mapping process.
const vector <Production*> * GEGrammar :: getProductions()
{

  #if (DEBUG_LEVEL >= 1)
    cerr << "'const DerivationTree* GEGrammar::getDerivationTree()' called\n";
  #endif

  if ((!getValidGrammar()) || (!genotype.getValid()) || (!getGenotype()-> size()))
  {
    return NULL;
  }

  genotype2Phenotype(true);
  return &productions;

}



/* ---- Strict implementation of genotype2Phenotype interface ---- */
// Calls local genotype2Phenotype(bool) method, with bool=false.
bool GEGrammar :: genotype2Phenotype()
{

  #if (DEBUG_LEVEL >= 1)
    cerr << "'bool GEGrammar::genotype2Phenotype()' called\n";
  #endif

  return genotype2Phenotype(false);

}



/* ---- Updates the contents of the phenotype structure ---- */
// based on the current genotype and the current grammar,
// and according to the standard GE mapping process.
// Returns true upon a successful mapping, and false
// otherwise, and also updates the valid field of the phenotype.
// With argument set to true, also updates derivationTree.
bool GEGrammar :: genotype2Phenotype (const bool buildDerivationTree)
{

  #if (DEBUG_LEVEL >= 1)
    cerr << "'bool GEGrammar::genotype2Phenotype(const bool)' called\n";
    cerr << "=====> Outputting BNF \n";
    outputBNF(cout);
    cerr << "======> Checking Genotype "<<endl;
    cerr << "======> isValid() : "<<genotype.getValid();
    cerr << " size = "<<genotype.size() << endl;
    Genotype :: iterator errGIt = genotype.begin();
    while (errGIt != genotype.end())
      cerr << *(errGIt++) << " ";
    cerr << endl;
  #endif

  bool returnValue = true;
  unsigned int newEffectiveSize = 0;
  // Start by setting effectiveSize to 0
  genotype.setEffectiveSize(newEffectiveSize);

  phenotype.clear();
  // Quick safety checks
  //if((!getValidGrammar())||(!genotype.getValid())||(!getGenotype()->size())){
  if (!getValidGrammar())
  {
    phenotype.clear();
    phenotype.setValid(false);
    return false;
  }

  // Wraps counter and nonterminals stack
  unsigned int wraps=0;
  vector<const Symbol*> nonterminals;

  // Iterators
  Genotype :: iterator genoIt = genotype.begin();

  //Pointers to a Rule and Production, each selected in each step of
  //the mapping process. These two pointers are required when
  // deriving a tree with Attribute Grammars; otherwise,
  // they are not required.
  Rule* rulePtr = NULL;
  int prodPtr = -1;
  bool gotToUseWrap = false;
  int result = -1;

  // Start with the start symbol
  nonterminals.push_back(getStartSymbol());

  if (!getUseAG())
  {

    if (buildDerivationTree)
    {
      productions.clear();
      // Use start symbol as the derivationTree node
      derivationTree.setData(getStartSymbol());
      //derivationTree.setData(symbolPtr);
    }

    // Get rid of all non-terminal symbols
    while ((!nonterminals.empty()) && (wraps <= getMaxWraps()))
    {
      //reset the pointers.
      rulePtr = NULL;
      prodPtr = -1;
      // Do a mapping step
      result = genotype2PhenotypeStep (nonterminals, genoIt, buildDerivationTree, rulePtr, prodPtr);
      returnValue = geno2phenoStepDecision (result, genoIt, newEffectiveSize, gotToUseWrap, wraps);
    }

    //newEffectiveSize+=(genoIt-genotype.begin());
    // Was the mapping successful?
    if ((wraps > getMaxWraps()) || (!nonterminals.empty()))
    {
      returnValue=false;
      // Add remaining symbols in nonterminals queue to phenotype
      while (!nonterminals.empty())
      {
        phenotype.push_back(nonterminals.back());
        nonterminals.pop_back();
      }
    }

    // Now build derivation tree, based on productions vector
    if (buildDerivationTree)
    {
      derivationTree.clear();
      derivationTree.setData (getStartSymbol());
      derivationTree.setCurrentLevel(1);
      derivationTree.setDepth(1);
      vector <Production*> :: iterator prodIt = productions.begin();
      buildDTree(derivationTree,prodIt);
    }

  }

  else
  { //if using Attribute Grammars
    agDtree-> clearTree();
    agDtree-> setTree (new AGTree ((AGSymbol*) createSymbol (*getStartSymbol(), true),1 ,1));
    //We also pass 'const Symbol*' corresponding to the node in AGTree because we need this information
    //to execute findRule() method.
    AGMapState state = buildAGTree (false, *(agDtree-> getTree()), getStartSymbol(), *agContext, nonterminals, genoIt, newEffectiveSize, wraps, gotToUseWrap, 0, 0);
    returnValue = (state==PASS);
    agDtree-> loadPhenotype(&phenotype);
    agContext-> setParent(NULL);
  }

  phenotype.setValid (returnValue);
  genotype.setEffectiveSize (newEffectiveSize);
  genotype.setWraps (wraps);

  return returnValue;

}



/* ---- geno2phenoStepDecision ---- */
bool GEGrammar :: geno2phenoStepDecision (int result, Genotype :: iterator& genoIt, unsigned int& newEffectiveSize, bool& gotToUseWrap, unsigned int& wraps)
{

  #if (DEBUG_LEVEL >= 2)
    cerr << "'bool GEGrammar::geno2phenoStepDecision (int, Genotype :: iterator, int&, bool&, int&)' called\n";
  #endif

  bool returnValue = true;
  switch (result)
  {
    case -1:returnValue=false;
      break;
    case 0: ;
      break;
    case 1: genoIt++;
      newEffectiveSize++;
      if (gotToUseWrap)
      {
        wraps++;
        gotToUseWrap=false;
      }
      // Check if wrap is needed
      if (*genoIt==*genotype.end())
      {
        //newEffectiveSize+=genotype.size();
        genoIt = genotype.begin();
        gotToUseWrap=true;
      }
      break;
    default: cerr << "Internal error in genotype2Phenotype().\n";
      cerr << "Execution aborted.\n";
      exit(0);
    }

    return returnValue;

}



/* ---- Updates the contents of the genotype structure ---- */
// based on the current phenotype and the current grammar,
// and according to a mapping process corresponding to the inverse
// of the standard GE mapping process.
// Returns true upon a successful inverse mapping, and false otherwise.
bool GEGrammar :: phenotype2Genotype()
{

  #if (DEBUG_LEVEL >= 2)
    cerr << "'bool GEGrammar :: phenotype2Genotype()' called\n";
  #endif

  cerr << "Method not yet implemented in this release of libGE.\n";
  return false;

  //FIXME

}



/* ---- Performs one step of the mapping process ---- */
// that is, maps the next non-terminal symbol on the nonterminals stack
// passed as argument, using the codon at the position pointed by genoIt.
// Returns number of codons consumed, -1 if not successful
int GEGrammar :: genotype2PhenotypeStep (vector <const Symbol*> &nonterminals, Genotype :: iterator genoIt, bool buildDerivationTree, Rule* &rulePtr, int& prodPtr, AGTree* currentAGNode)
{

  #if (DEBUG_LEVEL >= 1)
    cerr << "'bool GEGrammar::genotype2PhenotypeStep(stack<const Symbol*>&,Genotype::iterator&,bool)' called\n";
  #endif

  Rule :: iterator prodIt;
  int returnValue;

  // Find the rule for the current non-terminal
  rulePtr=findRule(*(nonterminals.back()));
  #if (DEBUG_LEVEL >= 1)
    cerr << "mapping " << *(nonterminals.top()) << " with " << *genoIt << "\n";
  cerr << "*(rulePtr->lhs.back()) = " << *(rulePtr->lhs.back())<<"\n";
  #endif

  //For the time being, use of "<GECodonValue> is not allowed for Attribute Grammars
  if (!rulePtr)
  {
    // Unknown symbol or special symbol that requires non-empty genotype
    // Include symbol on phenotype
    if (!getUseAG())
      phenotype.push_back (nonterminals.back());
    // Remove non-terminal
    nonterminals.front();
    // Invalidate mapping
    returnValue=-1;
  }

  // Allow recursive rules, but only if they consume a codon
  else if ((rulePtr-> getMinimumDepth() >= INT_MAX>>1) // Stuck on recursive rule
    && (rulePtr-> size()<=1)) { // No codon will be consumed
    // Include symbol on phenotype
    if (!getUseAG())
      phenotype.push_back (nonterminals.back());
    // Remove non-terminal
    nonterminals.pop_back();
    // Invalidate mapping
    returnValue = -1;
  }
  else
  {
    // Remove non-terminal
    nonterminals.pop_back();
    // Choose production
    if ((*genoIt==*genotype.end()) && (rulePtr-> size()>1))
    {
      // Empty genotype, but symbol requires choice
      // Include symbol on phenotype
      if (!getUseAG())
        phenotype.push_back (*(rulePtr->lhs.begin()));
      //phenotype.push_back(createSymbol(**(rulePtr->lhs.begin()),getUseAG()));
      // Invalidate mapping
      returnValue=-1;
    }
    else
    {
      if (*genoIt==*genotype.end())
      { //Empty genotype
        prodIt=rulePtr-> begin();
      }
      else
      {
        // prodIt=rulePtr->begin()+*genoIt%(rulePtr->size());
        prodPtr = chooseProduction (*genoIt, rulePtr, currentAGNode);

        //The following check is necessary because with AGs, chooseProduction
        //can return -1, and the following code will generate an invalid result
        //in that case.
        if (!getUseAG())
          prodIt = rulePtr-> begin() + prodPtr;
      }

      if (!getUseAG())
      {
        // Place production on productions vector
        if (buildDerivationTree)
        {
          productions.push_back(&*prodIt);
        }

        // Put all terminal symbols at start of production onto phenotype
        int s_start = 0;
        int s_stop = (*prodIt).size();
        while ((s_start<s_stop) && ((*prodIt)[s_start]-> getType()==TSymbol))
        {
          phenotype.push_back((*prodIt)[s_start]);
          //phenotype.push_back(createSymbol(*((*prodIt)[s_start]), getUseAG()));
          s_start++;
        }
        // Push all remaining symbols from production onto nonterminals queue, backwards
        for (; s_stop > s_start; s_stop--)
        {
          nonterminals.push_back ((*prodIt)[s_stop-1]);
          //nonterminals.push(createSymbol(*((*prodIt)[s_stop-1]), getUseAG()));
        }

      }

      // 0 or 1 choice for current rule, didn't consume genotype codon
      if (rulePtr->size()<=1)
      {
        returnValue=0;
      }
      else
      {
        returnValue = 1;
      }
    }

  }

  // Finally, pop all terminal symbols on top of stack and insert onto phenotype
  if (!getUseAG())
    while ((!nonterminals.empty()) && (nonterminals.back()-> getType() == TSymbol))
    {
      phenotype.push_back (nonterminals.back());
      nonterminals.pop_back();
    }

  return returnValue;

}



/* --- This method builds a Derivation Tree with AGs ---- */
// Unlike GEGrammar::buildDTree method this method can not make use of
// a pre-filled productions vector while developing a phenotype. Instead,
// the  construction of this Derivation Tree goes alongside mapping.
AGMapState GEGrammar :: buildAGTree (const bool init, AGTree& currentNode, const Symbol* nodeSym, AGContext& context, vector<const Symbol*>& nonterminals, Genotype :: iterator& genoIt, unsigned int& newEffectiveSize, unsigned int& wraps, bool& gotToUseWrap, int prevProdIndex, unsigned int backtrack, const bool growMethod,const unsigned int maxDepth, const int genoLenSoFar)
{

  #if (DEBUG_LEVEL >= 1)
    cerr << "'AGMapState GEGrammar::buildAGTree(AGTree&, const Symbol*, AGContext&, tack<const Symbol*>&, Genotype::iterator&, unsigned int&, unsigned int&, bool&, int, unsigned int)' called\n";
        cerr << "'=====> *(currentNode.getData()) = '"<<(*(currentNode.getData()))<<"'"<<endl;
  #endif

  //AGSymbol.h defines enum AGMapState {PASS,FAIL,CONSTRAINT_VIOLATION};

  // Stop condition while sensible initialisation
  if (init && currentNode.getCurrentLevel() > maxDepth)
    return FAIL;

  //If we hit a leaf, stop and return.
  if (currentNode.getData()-> getType() != NTSymbol)
  {
    return currentNode.getData()-> updateInheritedAttributes (context, prevProdIndex, *agLookUp);
  }

  //if the tree failed to map within maximum allowed wraps.
  if (!init && wraps>getMaxWraps())
    return FAIL;

  AGMapState state=PASS;

  //If NTSymol, update its inherited attributes first and check against constraint violation/FAIL.
  //First check if this call is due to backtracking, because in that case the inherited attributes
  //of this non-terminal have already been updated; don't repeat it. Repeating can be expensive
  //and/or error prone: e.g. it can expand a vector each time unnecessarily.
  if (backtrack==0)
  {
    state = currentNode.getData()-> updateInheritedAttributes (context, prevProdIndex, *agLookUp);
    if (state!=PASS)
      return state;
  }

  Rule* rulePtr = NULL;
  int prodPtr = -1;

  if (!init)
  { //in normal mapping outside sensible initialisation
    nonterminals.push_back(nodeSym);
    int result = genotype2PhenotypeStep (nonterminals, genoIt, false, rulePtr, prodPtr, &currentNode);
    if (!geno2phenoStepDecision (result, genoIt, newEffectiveSize, gotToUseWrap, wraps))
      return FAIL;
    //If no rule could be selected, then constraint violation must have happened earlier.
    if (prodPtr == -1)
      return CONSTRAINT_VIOLATION;
  }
  else
  { //Sensible Initialisation
    vector <int> possibleRules; // The possible rules to choose from
    state = getAGInitProductionSet (rulePtr, currentNode, maxDepth, growMethod, possibleRules);
    if (state != PASS)
      return state;

    prodPtr = possibleRules[static_cast <CodonType> (possibleRules.size()*(rand() / (RAND_MAX+1.0)))];
    // Save choice
    if (rulePtr->size()>1)
    {
      genotype.push_back(prodPtr);
      // Perform "unmod" on choice
      genotype.back()+=static_cast <CodonType> ((genotype.getMaxCodonValue() / rulePtr-> size()*(rand() / (RAND_MAX+1.0))))*rulePtr-> size();
    }
  }

  Production :: iterator symbIt = (rulePtr-> begin() + prodPtr)-> begin();
  while (&(*symbIt) != &(*(rulePtr-> begin() + prodPtr)-> end()))
  {
    currentNode.push_back (AGTree ((AGSymbol*) createSymbol (**symbIt,true), currentNode.getCurrentLevel()+1, currentNode.getDepth()+1));
    symbIt++;
  }

  //Save current state of context before defining a new state
  AGTree :: iterator prevStartIndex = context.getStartIndex();
  AGTree :: iterator prevEndIndex = context.getEndIndex();
  AGTree* prevParent = context.getParent();

  //Also save current value of genoIt, wraps, gotToUseWrap. This is required for backtracking when CONSTRAINT_VIOLATION happens.
  Genotype :: iterator prevGenoIt = genoIt;
  unsigned int prevWraps=wraps, prevNewEffectiveSize = newEffectiveSize;
  bool prevGotToUseWrap = gotToUseWrap;

  context.setParent (&currentNode);
  context.setStartIndex (currentNode.begin());
  // Expand each child node
  AGTree :: iterator treeIt = currentNode.begin();
  symbIt = (rulePtr-> begin() + prodPtr)-> begin();
  unsigned int currentMaxDepth = currentNode.getDepth(); //save the current max depth of this tree; can revert back if constraint violation occurs
  while (&(*treeIt) != &(*currentNode.end()) && state==PASS)
  {
    context.setEndIndex(treeIt);
    state = buildAGTree (init,*treeIt, *symbIt, context, nonterminals, genoIt, newEffectiveSize, wraps, gotToUseWrap, prodPtr, 0, growMethod, maxDepth, genoLenSoFar+1);
    //restore the start index, which is possibly set to some other part of the tree due to recursive call.
    context.setStartIndex(currentNode.begin());
    context.setParent(&currentNode);
    if ((*treeIt).getDepth() > currentNode.getDepth()) //update the max depth of this node, so it can trickle back to the top node ultimately
      currentNode.setDepth ((*treeIt).getDepth());
    treeIt++;
    symbIt++;
    }
    //Assuming everything passed, time to update synthesised attributes; thus the EndIndex should point to the end
    //of nodes contained in the 'currentNode'.
    context.setEndIndex(treeIt);

    //Restore the parent of 'currentNode' as passed into the buildAGTree() call currently executing.
    context.setParent(prevParent);

    switch (state)
    {
      case PASS:  //Everything is in order, so update the synthesised attributes of currentNode & return.
        state = currentNode.getData()-> updateSynthesisedAttributes (context, prodPtr, *agLookUp);
        break;
      case FAIL: //End of this tree. Return
        break;
      case CONSTRAINT_VIOLATION:
        //If there is only 1 production to choose from, we must only exit from this function call.
        //Also keep track of how often we have backtracked in the past. If we have backtracked
        //as often as the number of productions available, then do not backtrack any further for
        //this non-terminal. This restriction can be changed in later versions to detect if all
        //distinct productions have been tried for this non-terminal.
        if (rulePtr-> size() > 1 && backtrack < (rulePtr->size()-1))
        {
          //Collapse the tree generated below the currentNode.
          AGDerivationTree :: clearSubtreeBelow (currentNode, agDtree-> isRecycling());
          //If initialisng, also contract the genotype back
          if (init)
            for (int ww = genotype.size(); ww > genoLenSoFar; ww--)
              genotype.pop_back();

          //Restore the context of currentNode and parameters to this function call.
          context.setParent (prevParent);
          context.setStartIndex (prevStartIndex);
          context.setEndIndex(prevEndIndex);

          wraps=prevWraps;
          newEffectiveSize = prevNewEffectiveSize;
          gotToUseWrap = prevGotToUseWrap;
          genoIt = prevGenoIt;
          currentNode.setDepth (currentMaxDepth); //restore max depth before collapsing

          if (!init) ++genoIt;
            //Backtrack: select a new rule for currentNode and re-generate the tree.
            state = buildAGTree (init, currentNode, nodeSym, context, nonterminals, genoIt, newEffectiveSize, wraps, gotToUseWrap, prevProdIndex, backtrack+1, growMethod, maxDepth, genoLenSoFar);
        }
        break;
      default:
        cerr << "Internal error in buildGEGrammar :: buildAGTree (..arguments..).\n";
        cerr << "Execution aborted.\n";
        exit(0);

    }

    //restore context
    context.setParent(prevParent);
    context.setStartIndex(prevStartIndex);
    context.setEndIndex(prevEndIndex);

    if (init)
    {
      genotype.setValid (state==PASS);
      phenotype.setValid (state==PASS);
    }

    return state;

}



/* ---- Only a skeleton implementation ---- */
// provided only to avoid making GEGrammar an abstract class because
// it is instantiated elsewhere in the code.
// GEGrammarSI overrides this method to provide a useful implementation; // the method is required for sensible initialisation with
// Attribute Grammars.
[[maybe_unused]] AGMapState GEGrammar :: getAGInitProductionSet ([[maybe_unused]] Rule* &rulePtr, [[maybe_unused]] AGTree& currentAGNode, [[maybe_unused]] const unsigned int maxDepth, [[maybe_unused]] const bool growMethod,[[maybe_unused]] vector <int> &possibleRules)
{

  #if (DEBUG_LEVEL >= 1)
    cerr << "'bool GEGrammar::getAGInitProductionSet(Rule*, AGTree&, const unsigned int, const bool &, vector<int>&)' called\n";
    if (getUseAG())
    {
      cerr << "currentAGNode of type AGTree* to be mapped is : " << *(currentAGNode.getData()) << endl;
    }
  #endif

  return PASS;

}



/* ---- Builds the derivation tree ---- */
// when useAG==false, based on the productions vector.
// Arguments are current tree node, and iterator on productions vector.
void GEGrammar :: buildDTree (DerivationTree &currentNode, vector <Production*> :: iterator &prodIt)
{

  #if (DEBUG_LEVEL >= 2)
    cerr << "'bool GEGrammar::buildDTree(vector<Production*>::iterator&)' called\n";
  #endif

  // If current symbol is not a non-terminal, or if all productions
  // have been treated
  if ((currentNode.getData()-> getType() != NTSymbol) || (*prodIt  == *productions.end()))
  {
    // Correct productions iterator, as no production was read from it
    prodIt--;
    return;
  }

  // Create new tree level
  Production :: iterator symbIt = (*prodIt)-> begin();
  while (&(*symbIt) != &(*(*prodIt)-> end()))
  {
    currentNode.push_back (DerivationTree (*symbIt, currentNode.getCurrentLevel()+1, currentNode.getDepth()+1));
    symbIt++;
  }

  // Expand each child node
  DerivationTree :: iterator treeIt = currentNode.begin();
  while (&(*treeIt) != &(*currentNode.end()))
  {
    buildDTree (*treeIt,++prodIt);
    treeIt++;
  }

}



/* ---- This method returns an index to a production ---- */
// When useAG==false this works in the standard GE fashion and
// returns codon modulo no. of available productions.
// When useAG==true, it ascertains if forward checking is enabled
// by confirming currentAGNode->getData()->getValidProductionIndices()!=NULL.
// If forward checking is enabled, the method chooses from the valid
// productions. If none of the
// productions is valid, it returns -1.
// If forward checking is not enabled, that is,
//  currentAGNode->getData()->getValidProductionIndices()==NULL
// then it reverts to standard GE fashion and returns codon modulo no. of available
// productions.
int GEGrammar :: chooseProduction (int codon, Rule *rulePtr, AGTree* currentAGNode)
{

  #if (DEBUG_LEVEL >= 1)
    cerr << "'bool GEGrammar::chooseProduction(int,Rule*&, AGTree*)' called\n";
    if (getUseAG() && currentAGNode)
    {
      cerr << "currentAGNode of type AGTree* to be mapped is : " << *(currentAGNode-> getData()) << endl;
    }
  #endif

  if (!getUseAG() )
  {
    return codon%rulePtr-> size();
  }

  int choice = -1;
  const vector <bool> * validIndices = (currentAGNode-> getData()-> getValidProductionIndices());

  if (!validIndices)
    return codon%rulePtr-> size();

  int validCount = 0;
  unsigned int size = validIndices-> size();

  #if (DEBUG_LEVEL >= 1)
    cerr << "validindices are : ";
    for (int i=0; i<size; i++)
      cerr << (*validIndices)[i] << " ";
    cerr << endl;
  #endif

  for (unsigned int i=0; i<size; i++)
    validCount+= (*validIndices)[i];
  if (validCount==0)
    return -1;

  #if (DEBUG_LEVEL >= 1)
    cerr << "no of valid Prods (validCount) = " << validCount << endl;
  #endif

  choice = codon % validCount;
  validCount = 0;
  for (unsigned i=0; i<size; i++)
    if ((*validIndices)[i] && (validCount++)==choice)
    {
      choice=i;
      break;
    }
    #if (DEBUG_LEVEL >= 1)
      cerr << "----> In chooseProduction: choice is: " << choice<<endl;
    #endif

  return choice;

}



/* ---- This method overrides that in CFGrammar class ---- */
// If useAG==false, it passes on to CFGrammar::createSymbol method.
// Otherwise, it generates a suitable AGSymbol object by consulting
// the AGLookUp object, which is implemented by the end user
// for each problem.
Symbol* GEGrammar :: createSymbol (const Symbol& sym, bool grammarMade)
{

  #if (DEBUG_LEVEL >= 2)
    cerr << "'GEGrammar::createSymbol(const Symbol&, bool)' called\n";
    cerr << "createSymbol() : Symbol object is: " << sym << endl;
    cerr << "findRule(" << sym << ") fail?: "<< (!findRule(sym)) << endl;
  #endif

  if (useAG == false)
    return CFGrammar :: createSymbol(sym);
  else {
    unsigned int _size = 0;
    if (grammarMade && sym.getType() == NTSymbol)
    {
      Rule* r = findRule(sym);
      if (!r) {
        cerr << "In GEGrammar::createSymbol(const Symbol&, bool).";
        cerr << " Can not find a rule for non-terminal: '" << sym << "'";
        cerr << " Exiting..." << endl;
        exit(0);
      }
      _size=r-> size();
    }
    return agLookUp-> findAGSymbol (sym, _size, grammarMade);
  }

}



/* ---- Returns agLookUp ---- */
const AGLookUp* GEGrammar :: getAGLookUp() const
{

  #if (DEBUG_LEVEL >= 2)
    cerr << "'GEGrammar::getAGSymbol()' called\n";
  #endif

  return agLookUp;

}



/* ---- Returns agContext ---- */
const AGContext* GEGrammar :: getAGContext() const
{

  #if (DEBUG_LEVEL >= 2)
    cerr << "'GEGrammar::getAGContext()' called\n";
  #endif

  return agContext;

}



#endif

