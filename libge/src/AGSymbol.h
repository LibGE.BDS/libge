// AGSymbol.h -*- C++ -*-

#pragma once

/*------------------------------------------------------------------------------
 This class implements a symbol of an Attribute Grammar, be it a non-terminal
 or terminal symbol. Type cast as its super class Symbol, it is used both by
 the Rule class, to specify its left side non-terminal symbols, and by the
 Production class, to specify its  terminal and non-terminal symbols.
 It is also used by the Phenotype class, to specify phenotypic symbols.
 It can hold any data type, but all data types will be treated as text, so
 for example no arithmetic operations are possible.
------------------------------------------------------------------------------*/

//Forward Declaration due to circular dependency between AGSymbol and AGContext.
//AGSymbol requires AGContext; AGContext requires AGTree, which in turn needs
//AGSymbol.

class AGLookUp;
class AGContext;

#include <iostream>
#include <vector>
#include <string>
#include <sstream>

#include "libGEdefs.h"
#include "Symbol.h"

using namespace std;




//Return Types of Semantic Functions
enum AGMapState
{
	PASS, FAIL, CONSTRAINT_VIOLATION
};




/* ---- AGSymbol ----- */
class AGSymbol : public Symbol
{

	public:
		bool operator==(const AGSymbol&);
		AGSymbol(const string, const SymbolType);
		AGSymbol(const AGSymbol&);
		virtual void setInUse(bool);
		virtual ~AGSymbol();
		virtual const vector<bool>* getValidProductionIndices() const;
		virtual bool getInUse() const;
		virtual AGMapState updateSynthesisedAttributes (AGContext &,
				const int prodIndex, AGLookUp& lookUp)=0;
		virtual AGMapState updateInheritedAttributes (AGContext &,
				const int prodIndex, AGLookUp& lookUp)=0;
		template <class T>  AGSymbol &operator=(const T);

	protected:
		//This variable is useful for recycling AGSymbol objects. When
		//true, this indicates that this object is in use as a part of
		//an AGDerivationTree object; otherwise, the object is free to be
		//used else where.
		bool inUse;

};




/* ---- Copy newValue as textual contents of Symbol ---- */
template <class T> AGSymbol &AGSymbol :: operator = (const T newValue)
{
	//Pass it on to the superclass.
	Symbol :: operator= (newValue);
	return *this;
}



