// Production.h -*- C++ -*-

#pragma once

/*------------------------------------------------------------------------------
 This class implements the production of a grammar. It is used by the Rule
 class to specify the various productions which make up a rule.
------------------------------------------------------------------------------*/

#include <vector>
#include "libGEdefs.h"
#include "AGLookUp.h"

using namespace std;



/* ----- Production ---- */
class Production : public vector <Symbol*>
{

	public:
		Production(const unsigned int=0);
		Production(const Production &);
		~Production();
		void clear();
		bool getRecursive() const;
		void setRecursive (const bool);
		unsigned int getMinimumDepth() const;
		void setMinimumDepth (const unsigned int);
		static AGLookUp* aglookup;
		static void setAGLookUp (AGLookUp* lookup);
		static AGLookUp* getAGLookUp();
		friend ostream &operator<< (ostream &, const Production &);

	private:
		// Recursive nature of production
		bool recursive;
		// Minimum depth of parse tree for production to map to terminal symbol(s)
		unsigned int minimumDepth;

};



