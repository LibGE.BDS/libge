#pragma once

#pragma once
	#include "Mapper.h"   
	#include "Genotype.h" 
	#include "Phenotype.h"
	// "Grammar.h" -> abstract class
	#include "Rule.h"	
	#include "Symbol.h"	
	#include "Production.h"	
	#include "CFGrammar.h"	
	#include "AGDerivationTree.h"
	#include "Tree.h"	
	#include "GEGrammar.h"
	#include "GEGrammarSI.h"
	#include "Initialiser.h"
  #include "AGSymbol.h"
	#include "GenericAGSymbol.h"  
  #include "AGLookUp.h"
  #include "AGContext.h"
  // added to move these files from /EXAMPLES to /src 
	#include "GEListGenome.h"
	#include "InitFunc.h"
	//#ifdef GALIB_INCLUDEPATH
	//	#include "GE_MITGALIB.h"
	//#endif


// include illigal ga
#pragma once
	#include "GE_ILLIGALSGA.h"


