// AGDerivationTree.cpp -*- C++ -*-

#ifndef _AGDERIVATIONTREE_CPP_
#define _AGDERIVATIONTREE_CPP_

/*----------------------------------------------------------------------
 This class that implements an n-ary derivation tree containing AGSymbol
----------------------------------------------------------------------*/

#include <iostream>
#include <string>

#include "AGDerivationTree.h"



/* ---- Constructor; initialises depth and current level ---- */
AGDerivationTree :: AGDerivationTree (bool recycleNodes)
{

  #if (DEBUG_LEVEL >= 2)
    cerr << "'AGDerivationTree()' called\n";
  #endif

  tree=NULL;
  recycle=recycleNodes;

}



/* ---- Copy constructor ---- */
AGDerivationTree :: AGDerivationTree (const AGDerivationTree &copy)
{

  #if (DEBUG_LEVEL >= 2)
    cerr << "'AGDerivationTree::AGDerivationTree(const AGDerivationTree&)' called\n";
  #endif

  setTree(copy.getTree());
}



/* ---- Destructor ---- */
AGDerivationTree :: ~AGDerivationTree()
{

  #if (DEBUG_LEVEL >= 2)
    cerr << "'AGDerivationTree::~AGDerivationTree()' called\n";
  #endif

    clearTree();

}



/* ---- Destructor ---- */
bool AGDerivationTree :: isRecycling() const
{

  #if (DEBUG_LEVEL >= 2)
    cerr << "'bool AGDerivationTree::isRecycling()' called\n";
  #endif

    return recycle;

}




/* ---- Set new tree ---- */
// The user should ensure to release the memory that the 'tree'
// member variable previously points to.
void AGDerivationTree :: setTree (AGTree* newTree)
{

  #if (DEBUG_LEVEL >= 2)
    cerr << "'AGDerivationTree::setTree(AGTree*)' called\n";
  #endif

    tree=newTree;

}



/* ---- Returns member variable 'tree' ---- */
AGTree* AGDerivationTree :: getTree() const
{

  #if (DEBUG_LEVEL >= 2)
    cerr << "'AGTree* AGDerivationTree::getTree() const' called\n";
  #endif

  return tree;

}




/* ---- Clears the contents of 'tree' and deletes it ---- */
void AGDerivationTree :: clearTree()
{

  #if (DEBUG_LEVEL >= 2)
    cerr << "'void AGDerivationTree::clearTree()' called\n";
  #endif

  if (tree!=NULL) {
    traverseTree(*tree, CLEAR, recycle);
    delete tree;
  }

}



/* ---- Clears the subtree below this node ---- */
void AGDerivationTree :: clearSubtreeBelow (AGTree& node, bool recycleNodes)
{

  #if (DEBUG_LEVEL >= 2)
    cerr << "'void AGDerivationTree :: clearSubTreeBelow(AGTree&)' called\n";
  #endif

  AGTree :: iterator treeIt = node.begin();

  while (treeIt!=node.end()) {
    traverseTree (*treeIt, CLEAR, recycleNodes);
    treeIt++;
  }

}




/* ----  Prints the contents of 'tree' ---- */
void AGDerivationTree :: printTree()
{

  #if (DEBUG_LEVEL >= 2)
    cerr << "'void AGDerivationTree :: printTree()' called\n";
  #endif

  if (tree!=NULL)
    traverseTree (*tree, PRINT, recycle);
  else
    cout<<"NULL: nothing to print.";

}



/* ---- Loads the leaves of 'tree' into the Phenotype vector ---- */
void AGDerivationTree :: loadPhenotype (Phenotype* pheno)
{

  #if (DEBUG_LEVEL >= 1)
    cerr << "'void AGDerivationTree::loadPhenotype(Phenotype*)' called\n";
    cerr << "Phenotype* == NULL : "<<(pheno==NULL)<<endl;
  #endif

  if (tree!=NULL && pheno!=NULL)
    traverseTree (*tree, PHENOTYPE, recycle, pheno);

}




/* ---- Traverses the tree and acts as per 'TraversalReason' ---- */
// If clearing the tree it removes all the nodes:
// if recycle==false then this method destroys all the AGSymbol
// objects; otherwise, permits recycling.
void AGDerivationTree :: traverseTree (AGTree& currentNode, TraversalReason reason, bool recycleNodes, Phenotype* pheno)
{

  #if (DEBUG_LEVEL >= 1)
    cerr << "'void AGDerivationTree :: traverseTree (AGTree&, TraversalReason, Phenotype&)' called\n";
    string rsn=reason==CLEAR? "CLEAR" : (reason==PRINT? "PRINT" : "PHENOTYPE" );
    cerr << " reason = "<<rsn<<", *currentNode.getData()= "<<(*currentNode.getData())<<endl;
    cerr << " currentNode.size() = " << currentNode.size()<<endl;
    if (pheno!=NULL)
      cerr<<"pheno->size() ="<<pheno->size()<<endl;
  #endif

  if (reason == PRINT)
  {
    //if a non-terminal, print: "("
    if (currentNode.size()!=0) cout<<"(";
      cout<<currentNode.getData()<<" ";
  }
  //If we have hit a leaf or the tree does not expland below this node
  if (currentNode.size()==0)
  {
    if (reason == PHENOTYPE)
      //add to the phenotype.
      pheno-> push_back (currentNode.getData());
    return;
  }

  // Expand each child node
  AGTree :: iterator treeIt = currentNode.begin();

  while (treeIt!=currentNode.end())
  {
    traverseTree(*treeIt, reason, recycleNodes, pheno);
    treeIt++;
  }

  if (reason == PRINT)
    //print the closing bracket: ")"
    cout<<")";

  if (reason == CLEAR)
  {
    if (!recycleNodes)
      delete currentNode.getData();
    else
    {
      currentNode.getData()-> setInUse (false);
      currentNode.setData(NULL);
    }
    currentNode.setValid(false);
    currentNode.clear();
  }

}



#endif

