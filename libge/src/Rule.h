// Rule.h -*- C++ -*-

#pragma once

/*------------------------------------------------------------------------------
 This class implements a rule of a grammar. It is used by classes such as
 Grammar to specify grammars. It is composed of a left hand side,
 containing Symbol elements, and a right hand side, containing
 Production elements.
------------------------------------------------------------------------------*/

#include <vector>
#include "libGEdefs.h"
#include "Production.h"

using namespace std;



/* ---- Rule ---- */
class Rule : public vector <Production>
{

public:
		Rule (const unsigned int=0);
		Rule (const Rule &);
		~Rule();
		void clear();
		bool getRecursive() const;
		void setRecursive (const bool);
		unsigned int getMinimumDepth() const;
		void setMinimumDepth (const unsigned int);
		// The left-hand side of the rule
		vector<Symbol*> lhs;
		// static AGLookUp* aglookup;
		// static void setAGLookUp(AGLookUp* lookup);

	private:
		// Recursive nature of rule
		bool recursive;
		// Minimum depth of parse tree for production to map to terminal symbol(s)
		unsigned int minimumDepth;

};


