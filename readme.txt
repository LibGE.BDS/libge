Before install libge, check for dependencies
In Linux be sure some necessary stuff is installed:
~$ sudo apt-get install build-essentials


1) Install galib:
-> go into galib folder
~$ cd galib247/
~galib247$ make
~galib247$ sudo make install


2) Install libge:
-> go into libge folder
~$ cd libge/
~libge$ ./configure
~libge$ make
~libge$ sudo make install


3) Run examples:
- go to Examples folder
~$ cd Examples/
~Examples$ make
~Examples$ ./GEGCC
